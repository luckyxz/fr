# 基于虹软SDK实现的人脸识别服务

#### 项目介绍
基于虹软的免费SDK，实现人脸特征的提取、保存和校验等功能

#### 软件架构
springboot
thymeleaf
mybatis-plus
Mysql数据库



#### 安装教程

1. 注册虹软https://ai.arcsoft.com.cn/，申请SDK并下载SDK解压到到c:/facelib，主要用到两个dll文件，人脸检测和人脸识别
![输入图片说明](https://images.gitee.com/uploads/images/2018/1005/145133_8948272c_331488.jpeg "TIM截图20181005145049.jpg")
2. 修改application.properties里的虹软appid以及sdkkey，修改成你自己申请的

arcsoft_appid=HLYZDALKyakyKNLmsDZdpnnX2fEmVdmJbZD757sUfFbm
arcsoft_fd_sdkkey=AwdLiNskvNDZu5WUMCCrhysh4CSXqzLMbbBkoBF4eYfV
arcsoft_fr_sdkkey=AwdLiNskvNDZu5WUMCCrhytBhoVHMXjVPX1sSwm7TvHX

