package com.farmlei.facereg.common.utils;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created by Administrator on 2017/10/18.
 */
public class PropertiesUtils {
    private static PropertiesConfiguration configuration = null;

    /**
     * 获取配置信息
     */
    public static Configuration getConfig() {

        if (configuration == null) {
            synchronized (PropertiesUtils.class) {
                if (configuration == null) {
                    try {
                        configuration = new PropertiesConfiguration();
                        configuration.setEncoding("UTF-8");
                        configuration.load("application.properties");
                    } catch (ConfigurationException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return configuration;
    }
}
