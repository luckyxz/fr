package com.farmlei.facereg.modules.face;

public class FaceInfo {
    public int left;
    public int top;
    public int right;
    public int bottom;
    public int orient;
}
