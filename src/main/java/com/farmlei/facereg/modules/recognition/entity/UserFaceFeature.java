package com.farmlei.facereg.modules.recognition.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.farmlei.facereg.common.base.SuperEntity;
import java.sql.Blob;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 人脸特征信息
 * </p>
 *
 * @author farmlei
 * @since 2018-10-05
 */
@Data
@EqualsAndHashCode
@Accessors(chain = true)
public class UserFaceFeature {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId
    private Long userId;

    /**
     * 人脸特征信息
     */
    private byte[] faceFeature;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public byte[] getFaceFeature() {
        return faceFeature;
    }

    public void setFaceFeature(byte[] faceFeature) {
        this.faceFeature = faceFeature;
    }
}
