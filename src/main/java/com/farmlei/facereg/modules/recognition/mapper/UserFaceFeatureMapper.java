package com.farmlei.facereg.modules.recognition.mapper;

import com.farmlei.facereg.modules.recognition.entity.UserFaceFeature;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 人脸特征信息 Mapper 接口
 * </p>
 *
 * @author farmlei
 * @since 2018-10-05
 */
public interface UserFaceFeatureMapper extends BaseMapper<UserFaceFeature> {

}
