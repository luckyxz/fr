package com.farmlei.facereg.modules.recognition.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmlei.facereg.modules.recognition.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 人员信息 服务类
 * </p>
 *
 * @author farmlei
 * @since 2018-10-04
 */
public interface IUserInfoService extends IService<UserInfo> {
    /**
     * 分页查询用户列表
     * @return 分页对象
     */
    Page<UserInfo> selectPageVo(Page page);

    /**
     * 保存用户
     * @param userInfo 用户信息
     */
    void saveUser(UserInfo userInfo) throws Exception;

    /**
     * 人脸识别
     * @param recognizeImg 带识别的人脸照片
     * @return 匹配的用户信息
     * @throws Exception 异常
     */
    UserInfo recognizeUser(String recognizeImg) throws Exception;
}
