package com.farmlei.facereg.modules.recognition.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmlei.facereg.common.utils.PropertiesUtils;
import com.farmlei.facereg.modules.face.AFR_FSDK_FACEMODEL;
import com.farmlei.facereg.modules.face.ASVLOFFSCREEN;
import com.farmlei.facereg.modules.face.FaceInfo;
import com.farmlei.facereg.modules.face.utils.ArcsoftUtil;
import com.farmlei.facereg.modules.recognition.entity.UserFaceFeature;
import com.farmlei.facereg.modules.recognition.entity.UserInfo;
import com.farmlei.facereg.modules.recognition.mapper.UserFaceFeatureMapper;
import com.farmlei.facereg.modules.recognition.mapper.UserInfoMapper;
import com.farmlei.facereg.modules.recognition.service.IUserFaceFeatureService;
import com.farmlei.facereg.modules.recognition.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.configuration.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

/**
 * <p>
 * 人员信息 服务实现类
 * </p>
 *
 * @author farmlei
 * @since 2018-10-04
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {
    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserFaceFeatureMapper userFaceFeatureMapper;
    @Resource
    private IUserFaceFeatureService iUserFaceFeatureService;

    @Override
    public Page<UserInfo> selectPageVo(Page page) {
        return userInfoMapper.selectPageVo(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUser(UserInfo userInfo) throws Exception {
        //保存用户
        userInfoMapper.insert(userInfo);
        System.out.println(userInfo.getId());

        //先从图片中解析人脸，如果解析不到，则直接返回错误
        iUserFaceFeatureService.saveFaceFeature(userInfo.getId(), userInfo.getFaceImg());
    }

    @Override
    public UserInfo recognizeUser(String recognizeImg) throws Exception {
        List<UserFaceFeature> featureList = userFaceFeatureMapper.selectList(null);
        if (featureList != null && featureList.size() > 0) {
            Configuration config = PropertiesUtils.getConfig();
            String fileRoute = config.getString("fileRoute");
            String filePath = fileRoute.substring(0, fileRoute.indexOf("/")) + recognizeImg;

            BufferedImage img = ImageIO.read(new File(filePath));
            ASVLOFFSCREEN inputImg = ArcsoftUtil.changePic(img);
            FaceInfo[] faceInfosA = ArcsoftUtil.doFaceDetection(inputImg);
            if (faceInfosA == null || faceInfosA.length < 1) {
                throw new Exception("未检测到人脸");
            }

            AFR_FSDK_FACEMODEL faceModel = ArcsoftUtil.extractFRFeature(inputImg, faceInfosA[0]);// 如果照片里有多张人脸，则只取第一个
            if (faceModel == null) {
                throw new Exception("未检测到人脸");
            }

            for (UserFaceFeature userFaceFeature : featureList) {
                if (userFaceFeature.getFaceFeature() != null) {
                    AFR_FSDK_FACEMODEL faceModelFromDB = AFR_FSDK_FACEMODEL
                            .fromByteArray(userFaceFeature.getFaceFeature());
                    float cmp = ArcsoftUtil.toCompare(faceModel, faceModelFromDB);
                    //如果匹配度大于0.7，则认为是同一个人
                    if (cmp > 0.7) {
                        faceModel.freeUnmanaged();
                        faceModelFromDB.freeUnmanaged();
                        return userInfoMapper.selectById(userFaceFeature.getUserId());
                    }
                    faceModelFromDB.freeUnmanaged();
                }
            }
            faceModel.freeUnmanaged();
        }
        return null;
    }
}
